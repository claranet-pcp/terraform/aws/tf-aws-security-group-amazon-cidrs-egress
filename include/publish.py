import boto3
import hashlib
import json
import optparse
import os
import sys
import urllib2

def get_md5_sum(url, max_file_size=100*1024*1024):
    remote = urllib2.urlopen(url)
    hash = hashlib.md5()

    total_read = 0
    while True:
        data = remote.read(4096)
        total_read += 4096

        if not data or total_read > max_file_size:
            break

        hash.update(data)

    return hash.hexdigest()

aws_region = sys.argv[1]
sns_topic_arn = sys.argv[2]


client = boto3.client('sns',region_name=aws_region)

url = "https://ip-ranges.amazonaws.com/ip-ranges.json"
md5 = get_md5_sum(url)

message = json.dumps({
    'create-time': 'yyyy-mm-ddThh:mm:ss+00:00',
    'synctoken': '0123456789',
    'md5': md5,
    'url': url})

response = client.publish(
    TopicArn=sns_topic_arn,
    Message=message)

json.dump(response, sys.stdout)
