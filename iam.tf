# Lambda policy for logging
resource "aws_iam_role_policy" "lambda_manage_sg_logging_policy" {
  name = "${random_id.name.hex}-logging"
  role = aws_iam_role.lambda_manage_sg_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

# Lambda policy for managing a security group
resource "aws_iam_role_policy" "lambda_manage_sg_policy" {
  name = random_id.name.hex
  role = aws_iam_role.lambda_manage_sg_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeSecurityGroups",
        "ec2:AuthorizeSecurityGroupEgress",
        "ec2:RevokeSecurityGroupEgress"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

# Lambda role
resource "aws_iam_role" "lambda_manage_sg_role" {
  name_prefix = random_id.name.hex

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# SNS permission to invoke Lambda via topic AmazonIpSpaceChanged
resource "aws_lambda_permission" "manage_sg_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.manage_sg.arn
  principal     = "sns.amazonaws.com"
  source_arn    = "arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged"
}

# SNS permission to invoke Lambda via custom trigger (to trigger upon creation)
resource "aws_lambda_permission" "manage_sg_sns_trigger_update" {
  statement_id  = "AllowTriggerFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.manage_sg.arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.manage_sg_sns.arn
}
