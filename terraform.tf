terraform {
  required_version = "> 0.12.0"
}

data "aws_region" "current" {}

provider "aws" {
  region = "us-east-1"
  alias  = "us"
}
