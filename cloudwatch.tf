resource "aws_cloudwatch_metric_alarm" "errors" {
  alarm_name        = "${aws_lambda_function.manage_sg.function_name}-errors"
  alarm_description = "${aws_lambda_function.manage_sg.function_name} invocation errors"

  namespace   = "AWS/Lambda"
  metric_name = "Errors"

  dimensions = {
    FunctionName = "${aws_lambda_function.manage_sg.function_name}"
  }

  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 0
  period              = 60
  evaluation_periods  = 1

  alarm_actions = var.critical_alarm_actions
  ok_actions    = var.critical_ok_actions
}
