# SNS topic
resource "aws_sns_topic" "manage_sg_sns" {
  name = random_id.name.hex
}

# SNS subscription
resource "aws_sns_topic_subscription" "sns_topic_subscription" {
  provider  = aws.us
  topic_arn = "arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged"
  protocol  = "lambda"
  endpoint  = aws_lambda_function.manage_sg.arn
}

# SNS subscription to trigger an update
resource "aws_sns_topic_subscription" "sns_topic_subscription_trigger_update" {
  topic_arn = aws_sns_topic.manage_sg_sns.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.manage_sg.arn
}
