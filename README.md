# tf-aws-security-group-amazon-cidrs-egress

Provides an AWS security group with all AWS CIDR's added as egress rules on port 443 for the region it's created in. Creates a Lambda function that subscribes to `arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged` to update the security group when the Amazon IP space changes.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

## Basic Usage
```
module "amazon_cidrs_egress" {
  source  = "../modules/tf-aws-lambda-amazon-cidrs-egress"

  name    = "${var.envname}-amazon-cidrs"
  vpc_id  = "${module.vpc.vpc_id}"

  critical_alarm_actions = ["${aws_sns_topic.customer_slack.arn}"]
}
```

## Inputs
| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| vpc_id | ID of the VPC to create the security group | No default | Yes |
| name | The base name used for created resources | No default | Yes |
| random_name_byte_length | The byte length of the random id generator used for unique resource names | 4 | No |
| critical_alarm_actions | Alarm actions for CRITICAL level CloudWatch alarms (function failed) | No default | No |
| critical_ok_actions | OK actions for CRITICAL level CloudWatch alarms | No default | No |

## Outputs

| Name | Description |
|------|-------------|
| amazon_cidrs_egress_sg_id | ID of the security group that was created |
