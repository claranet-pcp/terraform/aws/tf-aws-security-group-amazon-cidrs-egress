variable "name" {
  description = "The base name used for created resources."
  type        = string
}

variable "random_name_byte_length" {
  description = "The byte length of the random id generator used for unique resource names."
  default     = 4
}

variable "vpc_id" {
  description = "ID of the VPC to create the security group."
  type        = string
}

variable "lambda_timeout" {
  description = "Lambda execution timeout in seconds."
  default     = 10
}

variable "critical_alarm_actions" {
  description = "Alarm actions for CRITICAL level CloudWatch alarms (function failed)"
  type        = list(string)
  default     = []
}

variable "critical_ok_actions" {
  description = "OK actions for CRITICAL level CloudWatch alarms"
  type        = list(string)
  default     = []
}
