# Auto updating security group
resource "aws_security_group" "amazon_cidrs_egress" {
  name        = random_id.name.hex
  description = "Contains all Amazon CIDRs to allow egress traffic access to AWS services."
  vpc_id      = var.vpc_id

  tags = {
    Name       = random_id.name.hex
    AutoUpdate = "true"
    Protocol   = "https"
  }
}
