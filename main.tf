# Lambda function

## create lambda package
data "archive_file" "lambda_package" {
  type        = "zip"
  source_file = "${path.module}/include/lambda.py"
  output_path = "${path.cwd}/.terraform/tf-aws-security-group-amazon-cidrs-egress-${md5(file("${path.module}/include/lambda.py"))}.zip"
}

resource "aws_lambda_function" "manage_sg" {
  filename         = "./.terraform/tf-aws-security-group-amazon-cidrs-egress-${md5(file("${path.module}/include/lambda.py"))}.zip"
  source_code_hash = data.archive_file.lambda_package.output_base64sha256
  function_name    = random_id.name.hex
  role             = aws_iam_role.lambda_manage_sg_role.arn
  handler          = "lambda.lambda_handler"
  runtime          = "python2.7"
  timeout          = var.lambda_timeout

  environment {
    variables = {
      SG_NAME = aws_security_group.amazon_cidrs_egress.name
    }
  }
}

resource "null_resource" "notify_sns_topic" {
  depends_on = [aws_lambda_function.manage_sg]

  provisioner "local-exec" {
    command = "python ${path.module}/include/publish.py ${data.aws_region.current.name} ${aws_sns_topic.manage_sg_sns.arn}"
  }
}
