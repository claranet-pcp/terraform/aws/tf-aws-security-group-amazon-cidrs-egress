resource "random_id" "name" {
  byte_length = var.random_name_byte_length
  prefix      = "${var.name}-"
}
